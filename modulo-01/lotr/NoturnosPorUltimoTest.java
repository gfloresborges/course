
import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NoturnosPorUltimoTest
{    
    @Test
    public void selecionarNoturnosPorUltimo(){
        NoturnosPorUltimo noturno = new NoturnosPorUltimo();
        Elfo e1 = new ElfoVerde("Verde1");
        Elfo e2 = new ElfoNoturno("Noturno1");
        Elfo e3 = new ElfoVerde("Verde2");
        Elfo e4 = new ElfoNoturno("Noturno2");
        Elfo e5 = new ElfoNoturno("Noturno3");
        Elfo e6 = new ElfoVerde("Verde3");

        Exercito exe = new Exercito();
        exe.alistar(e1);
        exe.alistar(e2);
        exe.alistar(e3);
        exe.alistar(e4);
        exe.alistar(e5);
        exe.alistar(e6);
        

        Exercito resultado = new Exercito();
        resultado.alistar(e1);
        resultado.alistar(e3);
        resultado.alistar(e6);
        resultado.alistar(e2);
        resultado.alistar(e4);
        resultado.alistar(e5);
        assertEquals(resultado.getExercitoDeElfo(), noturno.getOrdemDeAtaque(exe.getExercitoDeElfo()));
    }
}

