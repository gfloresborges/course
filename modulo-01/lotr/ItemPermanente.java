
public class ItemPermanente extends Item {
    
    public ItemPermanente(int qtd, String desc) {
        super(qtd, desc);
    }
    
    public void setQuantidade(int novaQtd) {
        this.quantidade = novaQtd > 0 ? novaQtd : 1;
    }
}
