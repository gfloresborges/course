import java.util.ArrayList;
import java.util.Arrays;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoDaLuzTest
{
    @Test
    public void elfoDaLuzNasceComEspadaNoInventario() {
        ElfoDaLuz elfo = new ElfoDaLuz("Legolas");
        Item espada = new Item(1, "Espada de Galvorn");
        
        assertEquals(espada, elfo.getItens().obtemItemNaPosicao(2));
    }
    
    @Test
    public void atacaComEspadaUmaVez() {
        // Arrange
        ElfoDaLuz elfo = new ElfoDaLuz("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        elfo.atacarComEspada(umDwarf);
        // Assert
        assertEquals(1, elfo.getExperiencia());
        assertEquals(1, elfo.getContadorDeAtaque());
        assertEquals(79.0, elfo.getVida(), 1e-8);
        assertEquals(Status.SOFREU_DANO, elfo.getStatus());
    }
    
    @Test
    public void atacaComEspadaDuasVezes() {
        // Arrange
        ElfoDaLuz elfo = new ElfoDaLuz("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        elfo.atacarComEspada(umDwarf);
        elfo.atacarComEspada(umDwarf);
        // Assert
        assertEquals(2, elfo.getExperiencia());
        assertEquals(2, elfo.getContadorDeAtaque());
        assertEquals(89.0, elfo.getVida(), 1e-8);
        assertEquals(Status.SOFREU_DANO, elfo.getStatus());
    }
    
    @Test
    public void atacaComEspadaTresVezes() {
        // Arrange
        ElfoDaLuz elfo = new ElfoDaLuz("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        elfo.atacarComEspada(umDwarf);
        elfo.atacarComEspada(umDwarf);
        elfo.atacarComEspada(umDwarf);
        // Assert
        assertEquals(3, elfo.getExperiencia());
        assertEquals(3, elfo.getContadorDeAtaque());
        assertEquals(68.0, elfo.getVida(), 1e-8);
        assertEquals(Status.SOFREU_DANO, elfo.getStatus());
    }
    
    @Test
    public void atacaComEspadaAteMorrer() {
        // Arrange
        ElfoDaLuz elfo = new ElfoDaLuz("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        for (int i = 0; i < 30; i++)
            elfo.atacarComEspada(umDwarf);
        
        // Assert
        assertEquals(17, elfo.getExperiencia());
        assertEquals(17, elfo.getContadorDeAtaque());
        assertEquals(0.0, elfo.getVida(), 1e-8);
        assertEquals(Status.MORTO, elfo.getStatus());
    }
    
    @Test
    public void elfoDaLuzNaoPodePerderEspadaDeGalvorn() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        feanor.perderItem(new Item(1, "Espada de Galvorn"));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item(1, "Arco"),
                    new Item(2, "Flecha"),
                    new Item(1, "Espada de Galvorn")
                ));
        assertEquals(esperado, feanor.getItens().getInventario());
    }

    @Test
    public void elfoDaLuzPodePerderArco() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        feanor.perderItem(new Item(1, "Arco"));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item(2, "Flecha"),
                    new Item(1, "Espada de Galvorn")
                ));
        assertEquals(esperado, feanor.getItens().getInventario());
    }

    @Test
    public void elfoDaLuzSoAtacaComUnidadeDeEspada() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.getItens().getInventario().get(2).setQuantidade(0);
        feanor.atacarComEspada(new Dwarf("Gul"));
        assertEquals(79, feanor.getVida(), 1e-8);
    }
    
}
