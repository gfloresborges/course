

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class EstatisticasInventarioTest {
    @Test
    public void calculaMediaDeInventarioVazio() {
        Inventario umInventario = new Inventario();
        
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertTrue(Double.isNaN(stats.mediaDeQtdDeItens()));
    }
        
    @Test
    public void calculaMediaDeQtdDeItensNoInventario() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
        
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertEquals(6.167, stats.mediaDeQtdDeItens(), 0.001);
    }
        
    @Test
    public void calculaMediaDeQtdIguaisDeItensNoInventario() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(2, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(2, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(2, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(2, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(2, "Corda"));
        
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertEquals(2, stats.mediaDeQtdDeItens(), 0.000001);
    }
    
    @Test
    public void calculaMedianaDeInventarioVazio() {
        Inventario umInventario = new Inventario();
        
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertTrue(Double.isNaN(stats.medianaDaQtdDeItens()));
    }
    
    
    @Test
    public void calculaMedianaDeInventarioComUmItem() {
        Inventario umInventario = new Inventario();
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
                
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertEquals(2, stats.medianaDaQtdDeItens(), 1e-8);
    }
    
    @Test
    public void calculaMedianaDaQtdDeItensNoInventarioTamanhoPar() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
        
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertEquals(4.5, stats.medianaDaQtdDeItens(), 0.001);
    }
    
    @Test
    public void calculaMedianaDaQtdDeItensNoInventarioTamanhoImpar() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(1, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
        umInventario.adicionarItemNoInventario(new Item(10, "Vela"));
        
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertEquals(5.0, stats.medianaDaQtdDeItens(), 0.001);
    }
    
    @Test
    public void calculaMedianaDaQtdIguaisDeItensNoInventario() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(2, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(2, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(2, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(2, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(2, "Corda"));
        
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertEquals(2, stats.medianaDaQtdDeItens(), 0.000001);
    }
    
    @Test
    public void itensComQtdAcimaDaMediaNoInventarioVazio() {
        Inventario umInventario = new Inventario();
        
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertEquals(0, stats.qtdItensAcimaDaMedia());
    }
    
    @Test
    public void itensComQtdAcimaDaMediaNoInventario() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(5, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(4, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(15, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(3, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(8, "Corda"));
        
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertEquals(2, stats.qtdItensAcimaDaMedia());
    }
    
    @Test
    public void nenhumItemComQtdAcimaDaMediaNoInventario() {
        Inventario umInventario = new Inventario();
        
        umInventario.adicionarItemNoInventario(new Item(2, "Adaga"));
        umInventario.adicionarItemNoInventario(new Item(2, "Escudo"));
        umInventario.adicionarItemNoInventario(new Item(2, "Bracelete"));
        umInventario.adicionarItemNoInventario(new Item(2, "Capa"));
        umInventario.adicionarItemNoInventario(new Item(2, "Bota"));
        umInventario.adicionarItemNoInventario(new Item(2, "Corda"));
        
        EstatisticasInventario stats = new EstatisticasInventario(umInventario);
        
        assertEquals(0, stats.qtdItensAcimaDaMedia());
    }
}
