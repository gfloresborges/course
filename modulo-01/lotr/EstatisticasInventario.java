
public class EstatisticasInventario
{
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario) {
        this.inventario = inventario;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public double mediaDeQtdDeItens() {
        if (this.inventario.getInventario().isEmpty())
            return Double.NaN;
        
        double soma = 0.0;
        for (int i = 0; i < this.inventario.getTamanhoInventario(); i++) {
            soma += this.inventario.obtemItemNaPosicao(i).getQuantidade();
        }
         
        return soma/this.inventario.getTamanhoInventario();
    }
    
    public double medianaDaQtdDeItens() {
        if (this.inventario.getInventario().isEmpty())
            return Double.NaN;
        
        this.inventario.ordenarInventario();
        int tamanhoInventario = this.inventario.getTamanhoInventario();
        int qtdMeio = this.inventario.obtemItemNaPosicao(tamanhoInventario/2).getQuantidade();
        double mediana = 0.0;        
                
        if (tamanhoInventario % 2 == 0) {
            mediana = this.inventario.obtemItemNaPosicao(tamanhoInventario/2-1).getQuantidade();
            mediana += this.inventario.obtemItemNaPosicao(tamanhoInventario/2).getQuantidade();
            mediana /= 2;
            return mediana;
        }
        return qtdMeio;
    }        
    
    public int qtdItensAcimaDaMedia() {
        double media = mediaDeQtdDeItens();
        int cont = 0;
        
        for (int i = 0; i < this.inventario.getTamanhoInventario(); i++) {
            double convert = (this.inventario.obtemItemNaPosicao(i).getQuantidade());
            if ( convert > media)
                cont++;
        }
        
        return cont;
    }
    
    
    
}
