public class Elfo extends Personagem {
    
    protected int experiencia = 0;
    protected int qtdExpQueRecebe = 1;
    private static int qtdElfos;
    
    public Elfo (String nome) {
        super(nome);
        this.vida = 100.0;
        this.qtdDanoQueRecebe = 0.0;
        itens.adicionarItemNoInventario(new Item(1, "Arco"));
        itens.adicionarItemNoInventario(new Item(2, "Flecha"));
        Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }    
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public Item getFlecha() {
        return this.itens.obtemItemNaPosicao(1);
    }
    
    public static int getQtdElfos() {
        return Elfo.qtdElfos;
    }
    
    public boolean temFlecha() {
        return getFlecha().getQuantidade() > 0;
    }
       
    protected void aumentarXp() {
        this.experiencia += this.qtdExpQueRecebe;
    }
    
    public void atirarFlecha(Dwarf dwarf) {
        if (temFlecha() && possuiVida()) {
            getFlecha().setQuantidade(getFlecha().getQuantidade()-1);
            this.aumentarXp();
            this.sofrerDano();
            dwarf.sofrerDano();
        }
    }        
}