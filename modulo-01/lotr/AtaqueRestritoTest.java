

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AtaqueRestritoTest {
    @Test
    public void ordenaAtaqueRestrito(){
        AtaqueRestrito restrito = new AtaqueRestrito();
        Elfo e1 = new ElfoNoturno("Noturno1");
        Elfo e2 = new ElfoNoturno("Noturno2");
        Elfo e3 = new ElfoVerde("Verde1");
        Elfo e4 = new ElfoVerde("Verde2");
        Elfo e5 = new ElfoVerde("Verde3");
        
        e1.getFlecha().setQuantidade(3);
        e2.getFlecha().setQuantidade(12);
        e3.getFlecha().setQuantidade(0);
        e4.getFlecha().setQuantidade(40);
        e5.getFlecha().setQuantidade(50);
        
        Exercito exe = new Exercito();
        exe.alistar(e1);
        exe.alistar(e2);
        exe.alistar(e3);
        exe.alistar(e4);
        exe.alistar(e5);

        Exercito resultado = new Exercito();
        resultado.alistar(e5);
        resultado.alistar(e4);
        resultado.alistar(e2);
        assertEquals(resultado.getExercitoDeElfo(), restrito.getOrdemDeAtaque(exe.getExercitoDeElfo()));
    }
}
