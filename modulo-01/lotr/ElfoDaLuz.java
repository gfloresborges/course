import java.util.ArrayList;
import java.util.Arrays;

public class ElfoDaLuz extends Elfo {
    
    private int contadorDeAtaque = 0;
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Arrays.asList("Espada de Galvorn"));
    
    public ElfoDaLuz(String nome) {
        super(nome);
        this.qtdDanoQueRecebe = 21.0;
        itens.adicionarItemNoInventario(new ItemPermanente(1, DESCRICOES_OBRIGATORIAS.get(0)));    
    }
    
    public int getContadorDeAtaque() {
        return this.contadorDeAtaque;
    }
    
    public void atacarComEspada(Dwarf dwarf) {
        if (this.possuiVida()) {
            this.contadorDeAtaque++;
            this.aumentarXp();
            if (this.contadorDeAtaque % 2 == 0)
                this.vida += 10.0;
            else 
                this.sofrerDano();
        }
    }
    
    public void perderItem(Item item) {
        if (!DESCRICOES_OBRIGATORIAS.contains(item.getDescricao()))
            super.perderItem(item);
    }
    
    
}
