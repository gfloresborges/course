import java.util.ArrayList;

public class AtaqueAlternado implements EstrategiaDeAtaque {
    private OrdenacaoExercito ordenar = new OrdenacaoExercito();
    public OrdenacaoExercito getOrdenar(){
        return this.ordenar;
    }

    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes) {
        ArrayList<Elfo> vivos = ordenar.selecionaVivos(atacantes);
        ArrayList<Elfo> verdes = ordenar.selecionaVerdes(vivos);
        ArrayList<Elfo> noturnos = ordenar.selecionaNoturnos(vivos);
        ArrayList<Elfo> ordenado = new ArrayList<Elfo>();
        
        for (int i = 0; i < verdes.size(); i++) {
            ordenado.add(verdes.get(i));
            ordenado.add(noturnos.get(i));
        }
        return ordenado;
    }

}
