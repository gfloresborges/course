import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    @After
    public void tearDown() {
        System.gc();
    }
        
    @Test
    public void elfoNasceComStatusRecemCriado() {
        Elfo elfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, elfo.getStatus());
    }
        
    @Test
    public void elfoNasceCom100DeVida() {
        Elfo elfo = new Elfo("Legolas");
        assertEquals(100.0, elfo.getVida(), 1e-8);
    }
    
    @Test
    public void elfoNasceComArcoEFlecha() {
        Elfo elfo = new Elfo("Legolas");
        Item arco = new Item(1, "Arco");
        Item flecha = new Item(2, "Flecha");
        
        assertEquals(arco, elfo.getItens().obtemItemNaPosicao(0));
        assertEquals(flecha, elfo.getItens().obtemItemNaPosicao(1));
    }
    
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXp() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        umElfo.atirarFlecha(umDwarf);
        // Assert
        assertEquals(1, umElfo.getExperiencia());
        assertEquals(1, umElfo.getFlecha().getQuantidade());
    }
    
    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXp() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        umElfo.atirarFlecha(umDwarf);
        umElfo.atirarFlecha(umDwarf);
        umElfo.atirarFlecha(umDwarf);
        // Assert
        assertEquals(2, umElfo.getExperiencia());
        assertEquals(0, umElfo.getFlecha().getQuantidade());
    }
    
    @Test
    public void verificaQtdInicialFlechaElfo() {
        // Arrange
        Elfo umElfo;
        // Act
        umElfo = new Elfo("Legolas");
        // Assert
        assertEquals(2, umElfo.getFlecha().getQuantidade());
    }
    
    @Test
    public void atirarFlechaEmDwarfTiraVida() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf umDwarf = new Dwarf("Bad Guy");
        // Act
        umElfo.atirarFlecha(umDwarf);
        // Assert
        assertEquals(100.0, umDwarf.getVida(), 1e-8);
        assertEquals(1, umElfo.getExperiencia());
        assertEquals(1, umElfo.getFlecha().getQuantidade());
    }
    
    public void criarUmElfoIncrementaContadorUmaVez() {
        new Elfo("Legolas");
        assertEquals(1, Elfo.getQtdElfos());
    }

    @Test
    public void criarDoisElfosIncrementaContadorDuasVezes() {
        new Elfo("Legolas");
        new Elfo("Legolas");
        assertEquals(2, Elfo.getQtdElfos());
    }

    @Test
    public void naoCriarElfoNaoIncrementaContador() {
        new Dwarf("Balin");
        assertEquals(0, Elfo.getQtdElfos());        
    }
}
