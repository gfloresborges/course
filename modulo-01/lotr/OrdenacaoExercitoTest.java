
import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class OrdenacaoExercitoTest {
    @Test 
    public void pegarApenasVivos(){
        NoturnosPorUltimo noturno = new NoturnosPorUltimo();
        Elfo e1 = new ElfoVerde("Verde1");
        Elfo e2 = new ElfoNoturno("Noturno1");
        Elfo e3 = new ElfoVerde("Verde2");
        Elfo e4 = new ElfoNoturno("Noturno2");
        Elfo e5 = new ElfoNoturno("Noturno3");
        Elfo e6 = new ElfoVerde("Verde3");
        for (int i =0; i < 50; i++) 
            e2.sofrerDano();        

        Exercito exe = new Exercito();
        exe.alistar(e1);
        exe.alistar(e2);
        exe.alistar(e3);
        exe.alistar(e4);
        exe.alistar(e5);
        exe.alistar(e6);

        Exercito resultado = new Exercito();
        resultado.alistar(e1);
        resultado.alistar(e3);
        resultado.alistar(e4);
        resultado.alistar(e5);
        resultado.alistar(e6);
        assertEquals(resultado.getExercitoDeElfo(), noturno.getOrdenar().selecionaVivos(exe.getExercitoDeElfo()));
    }

    @Test
    public void selecionarElfosVerdes(){
        NoturnosPorUltimo noturno = new NoturnosPorUltimo();
        Elfo e1 = new ElfoVerde("Verde1");
        Elfo e2 = new ElfoNoturno("Noturno1");
        Elfo e3 = new ElfoVerde("Verde2");
        Elfo e4 = new ElfoNoturno("Noturno2");
        Elfo e5 = new ElfoNoturno("Noturno3");
        Elfo e6 = new ElfoVerde("Verde3");

        Exercito exe = new Exercito();
        exe.alistar(e1);
        exe.alistar(e2);
        exe.alistar(e3);
        exe.alistar(e4);
        exe.alistar(e5);
        exe.alistar(e6);        

        Exercito resultado = new Exercito();
        resultado.alistar(e1);
        resultado.alistar(e3);
        resultado.alistar(e6);
        assertEquals(resultado.getExercitoDeElfo(), noturno.getOrdenar().selecionaVerdes(exe.getExercitoDeElfo()));
    }
    
    @Test
    public void selecionarElfosNoturnos(){
        NoturnosPorUltimo noturno = new NoturnosPorUltimo();
        Elfo e1 = new ElfoVerde("Verde1");
        Elfo e2 = new ElfoNoturno("Noturno1");
        Elfo e3 = new ElfoVerde("Verde2");
        Elfo e4 = new ElfoNoturno("Noturno2");
        Elfo e5 = new ElfoNoturno("Noturno3");
        Elfo e6 = new ElfoVerde("Verde3");

        Exercito exe = new Exercito();
        exe.alistar(e1);
        exe.alistar(e2);
        exe.alistar(e3);
        exe.alistar(e4);
        exe.alistar(e5);
        exe.alistar(e6);        

        Exercito resultado = new Exercito();
        resultado.alistar(e2);
        resultado.alistar(e4);
        resultado.alistar(e5);
        assertEquals(resultado.getExercitoDeElfo(), noturno.getOrdenar().selecionaNoturnos(exe.getExercitoDeElfo()));
    }
    

    @Test
    public void ordenaElfosPorQtdDeFlechas(){
        OrdenacaoExercito restrito = new OrdenacaoExercito();
        Elfo e1 = new ElfoVerde("Verde1");
        Elfo e2 = new ElfoNoturno("Noturno1");
        Elfo e3 = new ElfoVerde("Verde2");
        Elfo e4 = new ElfoNoturno("Noturno2");
        e1.getFlecha().setQuantidade(11);
        e2.getFlecha().setQuantidade(5);
        e3.getFlecha().setQuantidade(12);
        e4.getFlecha().setQuantidade(5);
        
        Exercito exe = new Exercito();
        exe.alistar(e1);
        exe.alistar(e2);
        exe.alistar(e3);
        exe.alistar(e4);        

        Exercito resultado = new Exercito();
        resultado.alistar(e3);
        resultado.alistar(e1);
        resultado.alistar(e2);
        resultado.alistar(e4);
        assertEquals(resultado.getExercitoDeElfo(), restrito.ordenaPorQtdFlecha(exe.getExercitoDeElfo()));
    }
}
