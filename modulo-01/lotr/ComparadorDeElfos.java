import java.util.*;

public class ComparadorDeElfos implements Comparator<Elfo> {
    public int compare(Elfo elfoAtual, Elfo proximoElfo) {
        if (elfoAtual.getClass() == proximoElfo.getClass())
            return 0;
        return elfoAtual instanceof ElfoVerde && proximoElfo instanceof ElfoNoturno ? -1 : 1;
    }
}
