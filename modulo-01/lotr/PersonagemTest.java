

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class PersonagemTest
{
    @Test
    public void personagemGanhaUmItem() {
        Personagem elfo = new Elfo("Legolas");
        Item espada = new Item(1, "Espada");
        elfo.ganharItem(espada);
        Item resultado = elfo.getItens().obtemItemNaPosicao(2);
        
        assertEquals(resultado, espada);        
    }
    
    @Test
    public void personagemGanhaDoisItem() {
        Personagem elfo = new Elfo("Legolas");
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        elfo.ganharItem(espada);
        elfo.ganharItem(escudo);
        Item resultado1 = elfo.getItens().obtemItemNaPosicao(2);
        Item resultado2 = elfo.getItens().obtemItemNaPosicao(3);
        
        assertEquals(resultado1, espada);
        assertEquals(resultado2, escudo);        
    }
    
    @Test
    public void personagemPerdeUmItem() {
        Personagem elfo = new Elfo("Legolas");
        
        elfo.perderItem(elfo.getItens().obtemItemNaPosicao(0));
        Item resultado = new Item(2, "Flecha");
        
        assertEquals(resultado, elfo.getItens().obtemItemNaPosicao(0));        
    }
}
