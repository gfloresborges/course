import java.util.ArrayList;

public class OrdenacaoExercito {
    public ArrayList<Elfo> selecionaVivos(ArrayList<Elfo> atacantes) {
        ArrayList<Elfo> vivos = new ArrayList<Elfo>();

        for (int i = 0; i < atacantes.size(); i++)
            if (atacantes.get(i).getStatus() != Status.MORTO)
                vivos.add(atacantes.get(i));

        return vivos;        
    }

    public ArrayList<Elfo> selecionaVerdes(ArrayList<Elfo> atacantes) {
        ArrayList<Elfo> verdes = new ArrayList<Elfo>();

        for (int i = 0; i < atacantes.size(); i++)
            if (atacantes.get(i) instanceof ElfoVerde)
                verdes.add(atacantes.get(i));

        return verdes;    
    }

    public ArrayList<Elfo> selecionaNoturnos(ArrayList<Elfo> atacantes) {
        ArrayList<Elfo> noturnos = new ArrayList<Elfo>();

        for (int i = 0; i < atacantes.size(); i++)
            if (atacantes.get(i) instanceof ElfoNoturno)
                noturnos.add(atacantes.get(i));

        return noturnos;    
    }

    public ArrayList<Elfo> selecionaVivosComFlecha(ArrayList<Elfo> atacantes) {
        ArrayList<Elfo> vivos = selecionaVivos(atacantes);
        ArrayList<Elfo> comFlecha = new ArrayList<Elfo>();

        for (int i = 0; i < vivos.size(); i++)
            if (vivos.get(i).temFlecha())
                comFlecha.add(vivos.get(i));

        return comFlecha;        
    }

    public ArrayList<Elfo> ordenaPorQtdFlecha(ArrayList<Elfo> atacantes) {
        ArrayList<Elfo> temp = new ArrayList<Elfo>(atacantes); 
        ArrayList<Elfo> ordenado = new ArrayList<Elfo>();

        if (!temp.isEmpty()) {
            Elfo elfoQtdMaior = temp.get(0);            
            while (temp.size() >= 2) {
                for (int i = 0; i < temp.size(); i++)
                    if (temp.get(i).getFlecha().getQuantidade() > elfoQtdMaior.getFlecha().getQuantidade())
                        elfoQtdMaior = temp.get(i);
                ordenado.add(elfoQtdMaior);
                temp.remove(elfoQtdMaior);
                elfoQtdMaior = temp.get(0);
            }
            ordenado.add(temp.get(0));
        }

        return ordenado;        
    }

}
