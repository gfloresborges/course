//maneira de encapsular funções. Inserindo eles em objetos.

var moedas = ( function () {

  function imprimir(num, milhar, decimal) {
    var up = Math.ceil(num*100)/100;
    var str = parseFloat(up).toFixed(2).split('.');
    var end = decimal + str[1];
    var count = 0
    var limit = 0;
  
    if (num < 0) {
      limit = 1;
    }
  
    for (let i = str[0].length-1; i >= limit; i--) {
      if (count < 3) {
        end = str[0][i] + end;
      }
      else {
        end = str[0][i] + milhar + end;
        count = 0;
      }
      count++
    }
  
    return end;
  }

  return {
    imprimirBRL: function (num) {
    if (num < 0) {
      return "-R$ " + imprimir(num, ".", ",");
    }
    return  "R$ " + imprimir(num, ".", ",");
    },

    imprimirGBP: function (num) {
      if (num < 0) {
        return "-£ " + imprimir(num, ",", ".");
      }
      return  "£ " + imprimir(num, ",", ".");
    },
    
    imprimirFR: function (num) {
      if (num < 0) {
        return "-" + imprimir(num, ".", ",") + " €";
      }
      return  imprimir(num, ".", ",") + " €";
    }
  }
})()

console.log(moedas.imprimirBRL(0)) // "R$ 0,00"
console.log(moedas.imprimirBRL(3498.99)) // “R$ 3.498,99”
console.log(moedas.imprimirBRL(-3498.99)) // “-R$ 3.498,99”
console.log(moedas.imprimirBRL(2313477.0135)) // “R$ 2.313.477,02”

console.log(moedas.imprimirGBP(0)) // "£ 0.00"
console.log(moedas.imprimirGBP(3498.99)) // “£ 3,498.99”
console.log(moedas.imprimirGBP(-3498.99)) // “-£ 3,498.99”
console.log(moedas.imprimirGBP(2313477.0135)) // “£ 2,313,477.02”

console.log(moedas.imprimirFR(0))
console.log(moedas.imprimirFR(3498.99))
console.log(moedas.imprimirFR(-3498.99))
console.log(moedas.imprimirFR(2313477.0135))
