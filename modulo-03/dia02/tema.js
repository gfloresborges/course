function arredondar(numero, precisao = 2) {
    const fator = Math.pow( 10, precisao )
    return Math.ceil( numero * fator ) / fator
}

function imprimirBRL(numero) {
    var qtdCasasMilhares = 3
    var separadorMilhar = '.'
    var separadorDecimal = ','

    var stringBuffer = []
    var parteDecimal = arredondar(Math.abs(numero)%1)
    var parteInteira = Math.trunc(numero)
    var parteInteiraString = Math.abs(parteInteira).toString()
    var parteInteiraTamanho = parteInteiraString.length

    var c = 1
    while (parteInteiraString.length > 0) {
        if ( c % qtdCasasMilhares === 0 ) {
            stringBuffer.push( `${ separadorMilhar }${ parteInteiraString.slice( parteInteiraTamanho - c ) }` )
            parteInteiraString = parteInteiraString.slice( 0, parteInteiraTamanho - c )
        } else if ( parteInteiraString.length < qtdCasasMilhares ) {
            stringBuffer.push( parteInteiraString )
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push( parteInteiraString )

    var decimalStr = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    return `${ parteInteira >= 0 ? 'R$ ' : '-R$ ' }${ stringBuffer.reverse().join('') }${ separadorDecimal }${ decimalStr }`
}

console.log(imprimirBRL(0)) // "R$ 0,00"
console.log(imprimirBRL(3498.99)) // “R$ 3.498,99”
console.log(imprimirBRL(-3498.99)) // “-R$ 3.498,99”
console.log(imprimirBRL(2313477.0135)) // “R$ 2.313.477,02”
