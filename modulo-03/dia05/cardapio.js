function cardapioTiaIFood( veggie = true, comLactose = false ) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]

  if ( comLactose ) {
    cardapio.push( 'pastel de queijo' )
  }

  cardapio = cardapio.concat( [
    'pastel de carne',
    'empada de legumes marabijosa'
  ] )

  if ( veggie ) {
    cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1 )
    cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
  }

  return cardapio
}

// esperado: [ 'cuca de uva', 'pastel de queijo', 'empada de legumes marabijosa' ]
console.log( cardapioTiaIFood() )
