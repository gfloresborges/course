import React from "react";
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import './meuBotao.css';

//outra maneira de declarar Component em React
const MeuBotao = ( { cor, texto, funcao, link, dadosNavegacao } ) =>
  <React.Fragment>
    <button className={ `btn ${ cor }` } onClick={ funcao }>
      { link ? <Link className="link" to={{ pathname: link, state: dadosNavegacao }}>{ texto }</Link> : texto }
    </button>
  </React.Fragment>

// export default class MeuBotao extends Component {

//   render() {
//     const { texto, cor, funcao } = this.props

//     return (
//     <React.Fragment>
//       <button className={ `btn ${ cor }` } onClick={ funcao }>{ texto }</button>
//     </React.Fragment>
//     )
//   }
// }

MeuBotao.propTypes = {
  texto: PropTypes.string.isRequired,
  funcao: PropTypes.func,
  dadosNavegacao: PropTypes.object,
  cor: PropTypes.oneOf( [ 'verde', 'azul', 'vermelho' ] ),
  link: PropTypes.string
}

MeuBotao.defaultProps = {
  cor: "verde"
}

export default MeuBotao
