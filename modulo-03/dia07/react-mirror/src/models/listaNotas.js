import Nota from "./nota";

export default class ListaNotas {
  constructor( notasDoServidor ) {
    this.todas = notasDoServidor.map( e => new Nota( e.nota, e.episodioId, e.id ))
  }

  // get avaliados() {
  //   return this.todas.map( e => )
  // }
}

