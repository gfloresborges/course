import EpisodiosApi from './episodiosApi'

export default class Episodio {
  constructor( id, nome, duracao, temporada, ordemEpisodio, thumbUrl, nota, avaliado ) {
    this.id = id;
    this.nome = nome;
    this.duracao = duracao;
    this.temporada = temporada;
    this.ordemEpisodio = ordemEpisodio;
    this.thumbUrl = thumbUrl;
    this.nota = nota;
    this.avaliado = avaliado;    
    this.qtdVezesAssistido = 0;
    this.episodiosApi = new EpisodiosApi();
  }

  validarNota( nota ) {
    nota = parseInt( nota )
    return  1 <= nota && nota <=5
  }

  async armazenarNota() {
    await this.episodiosApi.salvarNota( this.id, this.nota )
  }

  avaliar( nota ) {
    this.nota = parseInt( nota )
    this.avaliado = true
    // this.armazenarNota()
    return this.episodiosApi.registrarNota( this.id, this.nota )   
  }

  get duracaoEmMin() {
    return `${ this.duracao }min`
  }

  get temporadaEpisodio() {
    return `${ this.temporada.toString().padStart(2, '0') }/${ this.ordemEpisodio.toString().padStart(2, '0') }`
  }
  
}
