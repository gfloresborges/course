import axios from 'axios'

export default class EpisodiosApi {

  async default( id ) {
    await axios.patch( `http://localhost:9000/api/episodios/${ id }`, { nota: 0, avaliado: false } )
  }

  resetarDadosByDefault() {
    for ( let i = 1; i <= 19; i++ ) {
      this.default( i )      
    }
  }

  buscar() {
    return axios.get('http://localhost:9000/api/episodios')    
  }

  buscarNotas() {
    return axios.get('http://localhost:9000/api/notas')
  }

  salvarNota( id, nota ) {
    return axios.patch( `http://localhost:9000/api/episodios/${ id }`, { nota, avaliado: true } )
  }

  registrarNota( episodioId, nota ) {
    return axios.post( 'http://localhost:9000/api/notas', { nota, episodioId } )
  }
  
}
