import Episodio from "./episodio";

// inspiração: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random#Getting_a_random_integer_between_two_values
// essa função não está sendo exportada, logo ela é "privada"
function _sortear(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

export default class ListaEpisodios {
  constructor( episodiosDoServidor ) {
    this.todos = episodiosDoServidor.map( e => new Episodio( e.id, e.nome, e.duracao, e.temporada, e.ordemEpisodio, e.thumbUrl, e.nota, e.avaliado ))
  }

  get episodioAleatorio() {
    const indice = _sortear( 0, this.todos.length )
    return this.todos[ indice ]
  }
  
  get avaliados() {
    return this.todos.filter(e => e.avaliado)
  }

  marcarComoAssistido( episodio ) {
    const episodioParaMarcar = this.todos.find( e => e.nome === episodio.nome )
    episodioParaMarcar.assistido = true
    episodioParaMarcar.qtdVezesAssistido += 1
  }

}
