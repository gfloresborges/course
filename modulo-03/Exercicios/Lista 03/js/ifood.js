function cardapioTiaIFood( veggie = true, comLactose = false ) {
  var cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]

  if ( comLactose ) {
    cardapio.push( 'pastel de queijo' )
  }

  cardapio = cardapio.concat( ['pastel de carne', 'empada de legumes marabijosa'])

  if ( veggie ) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
    cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1 )
    cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
  }
  return cardapio
}

cardapioTiaIFood() // esperado: [ 'cuca de uva', 'pastel de queijo', 'empada de legumes marabijosa' ]
