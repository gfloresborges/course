var rodando = true;

function start(){
  if (rodando) {
    moment.locale('pt-BR');
    document.getElementById('germanySpan').innerText = getClock('Europe/Berlin');
    document.getElementById('italySpan').innerText = getClock('Europe/Rome');
    document.getElementById('franceSpan').innerText = getClock('Europe/Paris');
    document.getElementById('finlandSpan').innerText = getClock('Europe/Helsinki');
    document.getElementById('japanSpan').innerText = getClock('Asia/Tokyo');
    document.getElementById('greeceSpan').innerText = getClock('Europe/Athens');
    setTimeout(start, 1000);
  }
}

function stopTime() {
  rodando = false;
}

function clicou(pais, timezone) {
    alert(`${ pais } Timezone: ${selecionaTimezone(timezone)}`);
}

function selecionaTimezone(local) {
  return moment().tz(local).format('LLLL');
}

function getClock(local) {
  return moment().tz(local).format('LTS');
}
