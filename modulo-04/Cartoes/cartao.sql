drop table lancamento;
drop table cartao;
drop table cliente;
drop table loja_credenciador;
drop table loja;
drop table credenciador;
drop table emissor;
drop table bandeira;

create table loja(id integer not null primary key,
nome varchar2(100));

create table credenciador(id integer not null primary key,
nome varchar2(100));

create table loja_credenciador(id_loja integer not null references loja(id),
id_credenciador integer not null references credenciador(id),
taxa numeric(10,2) not null,
primary key (id_loja, id_credenciador)
);

create table bandeira(id integer not null primary key,
nome varchar2(100),
taxa numeric(10,2));

create table emissor(id integer not null primary key,
nome varchar2(100),
taxa numeric(10,2));

create table cliente(id integer not null primary key,
nome varchar2(100));

create table cartao(id integer not null primary key,
chip varchar2(50),
id_cliente integer not null references cliente(id),
id_bandeira integer not null references bandeira(id),
id_emissor integer not null references emissor(id),
vencimento date not null);

create table lancamento(id integer not null primary key,
id_cartao integer not null references cartao(id),
id_loja integer not null references loja(id),
id_credenciador integer not null references credenciador(id),
descricao varchar2(100) not null,
valor numeric (10,2) not null,
data_compra date not null);

create sequence loja_seq;
create sequence credenciador_seq;
create sequence bandeira_seq;
create sequence emissor_seq;
create sequence cartao_seq;
create sequence lancamento_seq;
create sequence cliente_seq;


insert into loja values(loja_seq.nextval, 'Pompéia');
insert into credenciador values (credenciador_seq.nextval, 'Redecard');
insert into loja_credenciador values(loja_seq.currval, credenciador_seq.currval, 10.0);
insert into bandeira values (bandeira_seq.nextval, 'Mastercard', 10.0);
insert into emissor values (emissor_seq.nextval, 'Itaú', 10.0);
insert into cliente values (cliente_seq.nextval, 'João');
insert into cartao values (cartao_seq.nextval, '123', cliente_seq.currval, bandeira_seq.currval, emissor_seq.currval, TO_DATE('2019/01/10', 'yyyy/mm/dd'));
insert into lancamento values (lancamento_seq.nextval, cartao_seq.currval, loja_seq.currval, credenciador_seq.currval, 'Material escolar', 100.0, sysdate);

select LANC.DATA_COMPRA, LANC.DESCRICAO, LANC.VALOR, 
(LC.TAXA / 100.0 * LANC.VALOR) as FATIA_CREDENCIADOR,
(BAND.TAXA / 100.0 * LANC.VALOR) as FATIA_BANDEIRA,
(EMI.TAXA / 100.0 * LANC.VALOR) as FATIA_EMISSOR
from LANCAMENTO LANC
inner join CARTAO CART ON CART.ID = LANC.ID_CARTAO
inner join LOJA LOJ on LOJ.ID = LANC.ID_LOJA
inner join BANDEIRA BAND on BAND.ID = CART.ID_BANDEIRA
inner join EMISSOR EMI ON EMI.ID = CART.ID_EMISSOR
inner join LOJA_CREDENCIADOR LC ON LANC.ID_LOJA = LC.ID_LOJA 
AND LC.ID_CREDENCIADOR = LANC.ID_CREDENCIADOR;