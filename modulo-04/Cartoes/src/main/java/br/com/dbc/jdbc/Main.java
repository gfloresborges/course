/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author guilherme.borges
 */
public class Main {
    
    public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'LOJA'").executeQuery();
            if (!rs.next())
            conn.prepareStatement(
                    "CREATE TABLE LOJA("
                            + "ID NUMBER NOT NULL PRIMARY KEY,"
                            + "NOME VARCHAR(100) NOT NULL)").execute();
                    } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na consulta do Main", ex);
            System.out.println("executado");
        }
        
    }
    
    
}
