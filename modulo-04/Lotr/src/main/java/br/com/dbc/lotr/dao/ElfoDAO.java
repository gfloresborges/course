/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;

/**
 *
 * @author guilherme.borges
 */
public class ElfoDAO extends AbstractDAO<ElfoJoin>{

    @Override
    protected Class<ElfoJoin> getEntityClass() {
        return ElfoJoin.class;
    }
    
    public ElfoJoin parseFrom(PersonagemDTO dto, Usuario usuario) {
        ElfoJoin elfoJoin = null;
        if(dto.getId() != null) {
            elfoJoin = buscar(dto.getId());
        }
        if( elfoJoin == null ) {
            elfoJoin = new ElfoJoin();
        }
        elfoJoin.setNome(dto.getNome());
        elfoJoin.setDanoElfo(dto.getDanoElfo());
        elfoJoin.setUsuario(usuario);
        
        return elfoJoin;
    }
    
    
    
}
