package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Usuario;

public interface ContatoRepository extends CrudRepository<Contato, Integer>{
	Contato findByValor(String valor);
	Contato findByUsuario(Usuario usuario);	
}
