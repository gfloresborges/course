package br.com.dbccompany.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Repository.BancoRepository;

@Service
public class BancoService {

	
	@Autowired
	public BancoRepository bancoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Banco salvar(Banco banco) {
		return bancoRepository.save(banco);
	}
	
	public Banco buscarBanco(Integer id) {
		if( bancoRepository.findById(id).isPresent() )
			return bancoRepository.findById(id).get();
		return null;
	}
	
	public List<Banco> allBancos() {
		return (List<Banco>) bancoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Banco editarBanco(Integer id, Banco banco) {
		banco.setId(id);
		return bancoRepository.save(banco);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarBanco(Banco banco) {
		bancoRepository.delete(banco);
	}
	
	public Banco buscarPorCodigo(Integer codigo) {
		return bancoRepository.findByCodigo(codigo);
	} 
}
