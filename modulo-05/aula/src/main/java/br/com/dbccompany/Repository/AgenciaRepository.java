package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Endereco;

public interface AgenciaRepository extends CrudRepository<Agencia, Integer>{
	Agencia findByCodigo(Integer codigo);
	Agencia findByNome(String nome);
	Agencia findByBanco(Banco banco);
	Agencia findByEndereco(Endereco endereco);
}
