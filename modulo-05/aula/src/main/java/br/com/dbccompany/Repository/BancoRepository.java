package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Banco;

public interface BancoRepository extends CrudRepository<Banco, Integer>{
	Banco findByCodigo(Integer codigo);
	Banco findByNome(String nome);
}
