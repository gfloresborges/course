package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "AGENCIA")
public class Agencia {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "ID_BANCO")
    private Banco banco;
    
    private String nome;
    
    private Integer codigo;
    
    @OneToOne
    @JoinColumn(name = "ID_GERENTE_GERAL")
    private Funcionario gerenteGeral;
    
    @OneToMany(mappedBy = "agencia")
    private List<Conta> contas = new ArrayList<>();
    
    @OneToOne
    @JoinColumn(name = "ID_ENDERECO")
	private Endereco endereco;
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void pushContas(Conta... contas) {
        this.contas.addAll(Arrays.asList(contas));
    }

	public Funcionario getGerenteGeral() {
		return gerenteGeral;
	}

	public void setGerenteGeral(Funcionario gerenteGeral) {
		this.gerenteGeral = gerenteGeral;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}    
      
}