package br.com.dbccompany.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Usuario salvar(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	
	private Usuario buscarUsuario(Integer id) {
		if ( usuarioRepository.findById(id).isPresent() )
			return usuarioRepository.findById(id).get();
		return null;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private Usuario editarUsuario(Integer id, Usuario usuario) {
		usuario.setId(id);
		return usuarioRepository.save(usuario);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void deletarUsuario(Usuario usuario) {
		usuarioRepository.delete(usuario);
	}
}
