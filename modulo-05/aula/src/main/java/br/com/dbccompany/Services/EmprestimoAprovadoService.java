package br.com.dbccompany.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.EmprestimoAprovado;
import br.com.dbccompany.Repository.EmprestimoAprovadoRepository;

@Service
public class EmprestimoAprovadoService {

	@Autowired
	private EmprestimoAprovadoRepository emprestimoAprovadoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private EmprestimoAprovado salvar(EmprestimoAprovado emprestimoAprovado) {
		return emprestimoAprovadoRepository.save(emprestimoAprovado);
	}
	
	private EmprestimoAprovado buscarEmprestimoAprovado(Integer id) {
		if ( emprestimoAprovadoRepository.findById(id).isPresent() )
			return emprestimoAprovadoRepository.findById(id).get();
		return null;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private EmprestimoAprovado editarEmprestimoAprovado(Integer id, EmprestimoAprovado emprestimoAprovado) {
		emprestimoAprovado.setId(id);
		return emprestimoAprovadoRepository.save(emprestimoAprovado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void deletarEmprestimoAprovado(EmprestimoAprovado emprestimoAprovado) {
		emprestimoAprovadoRepository.delete(emprestimoAprovado);
	}
}
