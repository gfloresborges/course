package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Entity.TipoMovimentacao;

public interface MovimentacaoRepository extends CrudRepository<Movimentacao, Integer>{
	List<Movimentacao> findByContaOrigem(Conta contaOrigem);
	List<Movimentacao> findByContaDestino(Conta contaDestino);	
	List<Movimentacao> findByData(String data);
	List<Movimentacao> findByTipoMovimentacao(TipoMovimentacao tipo);
}
