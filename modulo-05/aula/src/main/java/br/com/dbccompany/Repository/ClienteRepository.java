package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Conta;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	List<Cliente> findByContas(Conta conta);
}
