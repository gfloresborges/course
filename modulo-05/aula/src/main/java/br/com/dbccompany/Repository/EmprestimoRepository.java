package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Emprestimo;

public interface EmprestimoRepository extends CrudRepository<Emprestimo, Integer>{
	Emprestimo findByConta(Conta conta);	
}
