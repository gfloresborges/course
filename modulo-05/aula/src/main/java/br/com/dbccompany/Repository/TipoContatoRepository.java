package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.TipoContato;

public interface TipoContatoRepository extends CrudRepository<TipoContato, Integer>{
	TipoContato findByDescricao(String descricao);
}
