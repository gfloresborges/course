package br.com.dbccompany.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ContatoRepository;

@Service
public class ContatoService {

	@Autowired
	private ContatoRepository contatoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Contato salvar(Contato contato) {
		return contatoRepository.save(contato);
	}
	
	private Contato buscarContato(Integer id) {
		if ( contatoRepository.findById(id).isPresent() )
			return contatoRepository.findById(id).get();
		return null;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private Contato editarContato(Integer id, Contato contato) {
		contato.setId(id);
		return contatoRepository.save(contato);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void deletarContato(Contato contato) {
		contatoRepository.delete(contato);
	}
}
