package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "BANCO")
public class Banco {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(name = "CODIGO", unique = true)
	private Integer codigo;
	
	@Column(name = "NOME")
	private String nome;
	
	@OneToMany(mappedBy = "banco")
	private List<Agencia> agencias = new ArrayList<>();
	
	@OneToMany(mappedBy = "banco")
	private List<Funcionario> funcionarios = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Agencia> getAgencias() {
		return agencias;
	}

	public void pushAgencias(Agencia... agencias) {
		this.agencias.addAll(Arrays.asList(agencias));
	}
	
	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void pushFuncionarios(Funcionario... funcionarios) {
		this.funcionarios.addAll(Arrays.asList(funcionarios));
	}
	
	
}
