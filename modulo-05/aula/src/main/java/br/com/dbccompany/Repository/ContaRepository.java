package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.Funcionario;
import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Entity.TipoConta;

public interface ContaRepository extends CrudRepository<Conta, Integer>{
	Conta findByNumero(Integer numero);
	List<Conta> findByGerente(Funcionario gerente);
	List<Conta> findByClientes(Cliente cliente);
	List<Conta> findByLancamentosEfetuados(Movimentacao efetuada);
	List<Conta> findByLancamentosRecebidos(Movimentacao recebida);
	List<Conta> findByEmprestimos(Emprestimo emprestimo);
	List<Conta> findByTipoConta(TipoConta tipo);
}
