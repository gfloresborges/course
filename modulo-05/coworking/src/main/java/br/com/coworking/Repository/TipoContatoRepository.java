package br.com.coworking.Repository;

import br.com.coworking.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;

public interface TipoContatoRepository extends CrudRepository<TipoContato, Integer>{
	TipoContato findByNome(String nome);
}
