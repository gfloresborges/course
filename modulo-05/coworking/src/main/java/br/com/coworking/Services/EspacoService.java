package br.com.coworking.Services;

import br.com.coworking.Entity.Espaco;
import br.com.coworking.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacoService {
	
	@Autowired
	public EspacoRepository espacoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Espaco salvar(Espaco espaco) {
		return espacoRepository.save(espaco);
	}
	
	public Espaco buscarPorID(Integer id) {
		if( espacoRepository.findById(id).isPresent() )
			return espacoRepository.findById(id).get();
		return null;
	}
	
	public List<Espaco> buscarTodos() {
		return (List<Espaco>) espacoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Espaco editar(Integer id, Espaco espaco) {
		espaco.setId(id);
		return espacoRepository.save(espaco);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Espaco espaco) {
		espacoRepository.delete(espaco);
	}

}
