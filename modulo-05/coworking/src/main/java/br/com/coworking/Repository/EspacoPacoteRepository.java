package br.com.coworking.Repository;

import br.com.coworking.Entity.Espaco;
import br.com.coworking.Entity.EspacoPacote;
import br.com.coworking.Entity.Pacote;
import br.com.coworking.Enum.TipoContratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EspacoPacoteRepository extends CrudRepository<EspacoPacote, Integer>{
	List<EspacoPacote> findAllByTipoContratacao(TipoContratacao tipoContratacao);
	List<EspacoPacote> findAllByEspaco(Espaco espaco);
	List<EspacoPacote> findAllByPacote(Pacote pacote);
}
