package br.com.coworking.Controllers;

import br.com.coworking.Entity.SaldoCliente;
import br.com.coworking.Entity.SaldoClienteId;
import br.com.coworking.Services.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/saldocliente")
public class SaldoClienteController {

    @Autowired
    SaldoClienteService saldoClienteService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<SaldoCliente> listarTodos() {
        return saldoClienteService.buscarTodos();
    }

    @GetMapping(value = "/{idCliente}/{idEspaco}")
    @ResponseBody
    public SaldoCliente buscarPorID(@PathVariable Integer idCliente, @PathVariable Integer idEspaco) {
        return saldoClienteService.buscarPorID(new SaldoClienteId(idCliente, idEspaco));
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public SaldoCliente salvarNovo(@RequestBody SaldoCliente saldoCliente) {
        return saldoClienteService.salvar(saldoCliente);
    }

    @PutMapping(value = "/editar/{idCliente}/{idEspaco}")
    @ResponseBody
    public SaldoCliente editar(@PathVariable Integer idCliente, @PathVariable Integer idEspaco, @RequestBody SaldoCliente saldoCliente) {
        return saldoClienteService.editar(new SaldoClienteId(idCliente, idEspaco), saldoCliente);
    }
}
