package br.com.coworking.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {

    @Column(name = "ID_CLIENTE")
    private Integer idCliente;

    @Column(name = "ID_ESPACO")
    private Integer idEspaco;

    public SaldoClienteId() {}

    public SaldoClienteId(Integer idCliente, Integer idEspaco) {
        this.idCliente = idCliente;
        this.idEspaco = idEspaco;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public Integer getIdEspaco() {
        return idEspaco;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (this.getClass() != obj.getClass())
            return false;
        SaldoClienteId objeto = (SaldoClienteId) obj;
        if (objeto.getIdCliente() == this.idCliente && objeto.getIdEspaco() == this.idEspaco)
                return true;
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 21 * idCliente * idEspaco;
        return hash;
    }

}
