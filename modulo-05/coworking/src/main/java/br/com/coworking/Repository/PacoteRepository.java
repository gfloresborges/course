package br.com.coworking.Repository;

import br.com.coworking.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PacoteRepository extends CrudRepository<Pacote, Integer>{
	List<Pacote> findByValor(Double valor);
}
