package br.com.coworking.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.coworking.Entity.Cliente;
import br.com.coworking.Entity.Contratacao;
import br.com.coworking.Entity.Espaco;
import br.com.coworking.Enum.TipoContratacao;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Integer>{
	List<Contratacao> findAllByEspaco(Espaco espaco);
	List<Contratacao> findAllByCliente(Cliente cliente);
	List<Contratacao> findAllByTipoContratacao(TipoContratacao tipoContratacao);
}
