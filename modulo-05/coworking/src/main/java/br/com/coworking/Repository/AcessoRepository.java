package br.com.coworking.Repository;

import br.com.coworking.Entity.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.sql.Date;
import java.util.List;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {
    List<Acesso> findAllByData(Date data);
}
