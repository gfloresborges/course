package br.com.coworking.Repository;

import br.com.coworking.Entity.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
	Usuario findByNome(String nome);
	Usuario findByEmail(String email);
}
