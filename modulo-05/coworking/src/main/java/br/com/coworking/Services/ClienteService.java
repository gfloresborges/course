package br.com.coworking.Services;

import br.com.coworking.Entity.Cliente;
import br.com.coworking.Exceptions.ContatoInvalidoException;
import br.com.coworking.Repository.ClienteRepository;
import br.com.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService {

	@Autowired
	public ClienteRepository clienteRepository;

	@Autowired
	public ContatoRepository contatoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Cliente salvar(Cliente cliente) throws ContatoInvalidoException{
		List<String> tiposContato = new ArrayList<String>();

		//armazena na variavel tiposContato a lista de tipos de contato do cliente validando os contatos no banco de dados
		cliente.getContatos().forEach(e -> tiposContato.add(contatoRepository.findById(e.getId()).get().getTipoContato().getNome().toLowerCase()));

		if ( tiposContato.contains("email") && tiposContato.contains("telefone") )
			return clienteRepository.save(cliente);
		else {
			throw new ContatoInvalidoException();
		}
	}
	
	public Cliente buscarPorID(Integer id) {
		if( clienteRepository.findById(id).isPresent() )
			return clienteRepository.findById(id).get();
		return null;
	}
	
	public List<Cliente> buscarTodos() {
		return (List<Cliente>) clienteRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cliente editar(Integer id, Cliente cliente) {
		cliente.setId(id);
		return clienteRepository.save(cliente);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Cliente cliente) {
		clienteRepository.delete(cliente);
	}
	
}
