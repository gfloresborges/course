package br.com.coworking.Repository;

import br.com.coworking.Entity.Cliente;
import br.com.coworking.Entity.Contato;
import br.com.coworking.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer>{
	List<Contato> findAllByTipoContato(TipoContato tipoContato);
	List<Contato> findAllByValor(String valor);
	List<Contato> findAllByCliente(Cliente cliente);
}
