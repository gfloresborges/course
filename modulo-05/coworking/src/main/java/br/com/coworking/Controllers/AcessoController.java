package br.com.coworking.Controllers;

import br.com.coworking.Entity.Acesso;
import br.com.coworking.Entity.SaldoCliente;
import br.com.coworking.Entity.SaldoClienteId;
import br.com.coworking.Exceptions.SaldoInsuficienteException;
import br.com.coworking.Services.AcessoService;
import br.com.coworking.Services.SaldoClienteService;
import br.com.coworking.Services.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;

@Controller
@RequestMapping("/api/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @Autowired
    UtilService utilService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Acesso> listarTodos() {
        return acessoService.buscarTodos();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public Acesso buscarPorID(@PathVariable Integer id) {
        return acessoService.buscarPorID(id);
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public String salvarNovo(@RequestBody Acesso acesso) {
        SaldoClienteId saldoClienteId = acesso.getSaldoCliente().getId();
        SaldoCliente saldoCliente = utilService.validarSaldoCliente(saldoClienteId);;

        try {
            if( acesso.getIsEntrada() ) {
                if( acesso.getData() == null ) {
                    acesso.setData(new Date(System.currentTimeMillis()));
                }
                if( saldoCliente == null ) throw new SaldoInsuficienteException();
                acessoService.salvar(acesso);
            } else {
                //fazer regra para quando for saida
            }
        } catch (SaldoInsuficienteException e) {
            return e.getMENSAGEM();
        }
        return "Saldo atual de " + saldoCliente.getQuantidade() + " " + saldoCliente.getTipoContratacao();
    }

}
