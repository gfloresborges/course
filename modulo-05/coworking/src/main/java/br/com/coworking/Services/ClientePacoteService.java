package br.com.coworking.Services;

import br.com.coworking.Entity.ClientePacote;
import br.com.coworking.Repository.ClientePacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientePacoteService {

	@Autowired
	public ClientePacoteRepository clientePacoteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public ClientePacote salvar(ClientePacote clientePacote) {
		return clientePacoteRepository.save(clientePacote);
	}
	
	public ClientePacote buscarPorID(Integer id) {
		if( clientePacoteRepository.findById(id).isPresent() )
			return clientePacoteRepository.findById(id).get();
		return null;
	}
	
	public List<ClientePacote> buscarTodos() {
		return (List<ClientePacote>) clientePacoteRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ClientePacote editar(Integer id, ClientePacote clientePacote) {
		clientePacote.setId(id);
		return clientePacoteRepository.save(clientePacote);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(ClientePacote clientePacote) {
		clientePacoteRepository.delete(clientePacote);
	}
	
}
