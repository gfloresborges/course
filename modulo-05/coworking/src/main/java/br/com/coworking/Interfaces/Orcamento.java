package br.com.coworking.Interfaces;

import br.com.coworking.Entity.Espaco;
import br.com.coworking.Enum.TipoContratacao;

public interface Orcamento {

    Espaco getEspaco();
    TipoContratacao getTipoContratacao();
    Integer getQuantidade();
    Integer getPrazo();

}
