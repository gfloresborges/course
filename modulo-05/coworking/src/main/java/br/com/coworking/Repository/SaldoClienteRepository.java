package br.com.coworking.Repository;

import br.com.coworking.Entity.SaldoCliente;
import br.com.coworking.Entity.SaldoClienteId;
import br.com.coworking.Enum.TipoContratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, SaldoClienteId> {
    List<SaldoCliente> findAllByTipoContratacao(TipoContratacao tipoContratacao);
}
