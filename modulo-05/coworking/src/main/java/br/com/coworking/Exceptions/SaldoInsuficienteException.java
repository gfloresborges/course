package br.com.coworking.Exceptions;

public class SaldoInsuficienteException extends Exception {
    private final String MENSAGEM = "Saldo Insuficiente";

    public String getMENSAGEM() {
        return MENSAGEM;
    }
}
