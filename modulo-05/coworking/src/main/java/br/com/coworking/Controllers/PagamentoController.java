package br.com.coworking.Controllers;

import br.com.coworking.Entity.ClientePacote;
import br.com.coworking.Entity.Contratacao;
import br.com.coworking.Entity.Pagamento;
import br.com.coworking.Exceptions.PagamentoInvalidoException;
import br.com.coworking.Services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentoController {
	
	@Autowired
    PagamentoService pagamentoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Pagamento> listarTodos() {
		return pagamentoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Pagamento buscarPorID(@PathVariable Integer id) {
		return pagamentoService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public String salvarNovo(@RequestBody Pagamento pagamento) {
		ClientePacote pacote = pagamento.getClientePacote();
		Contratacao contratacao = pagamento.getContratacao();

		try {
			if (pacote == null && contratacao == null) throw new PagamentoInvalidoException();
			if (pacote != null && contratacao != null) throw new PagamentoInvalidoException();
			pagamentoService.salvar(pagamento);
		} catch (PagamentoInvalidoException e) {
			return e.getMENSAGEM();
		}
		return "Pagamento registrado com sucesso";
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Pagamento editar(@PathVariable Integer id, @RequestBody Pagamento pagamento) {
		return pagamentoService.editar(id, pagamento);
	}
}
