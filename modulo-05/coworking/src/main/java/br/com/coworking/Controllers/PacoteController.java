package br.com.coworking.Controllers;

import br.com.coworking.Entity.Pacote;
import br.com.coworking.Services.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController {
	
	@Autowired
    PacoteService pacoteService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Pacote> listarTodos() {
		return pacoteService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Pacote buscarPorID(@PathVariable Integer id) {
		return pacoteService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Pacote salvarNovo(@RequestBody Pacote pacote) {
		return pacoteService.salvar(pacote);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Pacote editar(@PathVariable Integer id, @RequestBody Pacote pacote) {
		return pacoteService.editar(id, pacote);
	}
}
