package br.com.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "ACESSO")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Acesso.class)
public class Acesso {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "IS_ENTRADA")
    private Boolean isEntrada;

    private Date data;

    @Column(name = "IS_EXCECAO")
    private Boolean isExcecao;

    @ManyToOne
    @JoinColumns({
    	@JoinColumn(name = "ID_CLIENTE"),
    	@JoinColumn(name = "ID_ESPACO")})
    private SaldoCliente saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsEntrada() {
        return isEntrada;
    }

    public void setIsEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getIsExcecao() {
        return isExcecao;
    }

    public void setIsExcecao(Boolean excecao) {
        isExcecao = excecao;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
