package br.com.coworking.Repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.coworking.Entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	Cliente findByNome(String nome);
	Cliente findByCpf(String cpf);
	List<Cliente> findByDataNascimento(Date data);
}
