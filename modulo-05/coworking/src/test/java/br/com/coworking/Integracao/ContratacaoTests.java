package br.com.coworking.Integracao;

import br.com.coworking.Controllers.*;
import br.com.coworking.CoworkingApplicationTests;
import br.com.coworking.Entity.*;
import br.com.coworking.Enum.TipoContratacao;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
public class ContratacaoTests extends CoworkingApplicationTests {

	private MockMvc mvc;

	TipoContato email = new TipoContato();
	TipoContato telefone = new TipoContato();
	Contato contato1 = new Contato();
	Contato contato2 = new Contato();
	Cliente cliente1 = new Cliente();
	Cliente cliente2 = new Cliente();
	Cliente cliente3 = new Cliente();
	Espaco espaco1 = new Espaco();
	Espaco espaco2 = new Espaco();
	Espaco espaco3 = new Espaco();
	Contratacao contratacaoA = new Contratacao();
	Contratacao contratacaoB = new Contratacao();
	Contratacao contratacaoC = new Contratacao();

	@Autowired
	TipoContatoController tipoContatoController;

	@Autowired
	ContatoController contatoController;

	@Autowired
	ClienteController clienteController;

	@Autowired
	EspacoController espacoController;

	@Autowired
	ContratacaoController contratacaoController;

	@Autowired
	ApplicationContext context;

	@Autowired
	ObjectMapper mapper;

	@Before
	public void setUp() throws Exception{
		this.mvc = MockMvcBuilders.standaloneSetup(tipoContatoController, contatoController, clienteController, espacoController, contratacaoController).build();

		//salvando tipoContato
		email.setNome("email");
		telefone.setNome("telefone");
		this.mvc.perform(post("/api/tipocontato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(email)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/tipocontato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(telefone)))
				.andExpect(MockMvcResultMatchers.status().isOk());

		//salvando contatos
		contato1.setValor("dbc@gmail.com");
		contato1.setTipoContato(new TipoContato());
		contato1.getTipoContato().setId(1);
		contato2.setValor("959565659");
		contato2.setTipoContato(new TipoContato());
		contato2.getTipoContato().setId(2);
		this.mvc.perform(post("/api/contato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(contato1)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/contato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(contato2)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		Contato contatoEmail = new Contato();
		contatoEmail.setId(1);
		Contato contatoTelefone = new Contato();
		contatoTelefone.setId(2);

		//criando cliente1
		cliente1.setNome("DBC");
		cliente1.setCpf("59521908477");
		cliente1.setDataNascimento(new Date(System.currentTimeMillis()));
		cliente1.pushContatos(contatoEmail, contatoTelefone);

		//criando cliente2
		cliente2.setNome("DBCServer");
		cliente2.setCpf("590484080");
		cliente2.setDataNascimento(new Date(System.currentTimeMillis()));
		cliente2.pushContatos(contatoEmail, contatoTelefone);

		//criando cliente3
		cliente3.setNome("Sicred");
		cliente3.setCpf("590590951");
		cliente3.setDataNascimento(new Date(System.currentTimeMillis()));
		cliente3.pushContatos(contatoEmail, contatoTelefone);

		//criando espaco1
		espaco1.setNome("Mind Room");
		espaco1.setQtdPessoas(20);
		espaco1.setValor(100.0);

		//criando espaco2
		espaco2.setNome("Cozinha");
		espaco2.setQtdPessoas(15);
		espaco2.setValor(150.0);

		//criando espaco3
		espaco3.setNome("Laboratorio");
		espaco3.setQtdPessoas(10);
		espaco3.setValor(200.0);
	}

	@Test
	public void eBuscarTodos() throws Exception{
		this.mvc.perform(get("/api/contratacao/"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void salvarContratacaoAComDesconto() throws Exception {
		this.mvc.perform(post("/api/espaco/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(espaco1))).andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/cliente/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(cliente1)))
				.andExpect(MockMvcResultMatchers.status().isOk());

		Espaco espacoNoBanco =  new Espaco();
		espacoNoBanco.setId(1);
		Cliente clienteNoBanco = new Cliente();
		clienteNoBanco.setId(1);

		contratacaoA.setEspaco(espacoNoBanco);
		contratacaoA.setCliente(clienteNoBanco);
		contratacaoA.setTipoContratacao(TipoContratacao.HORA);
		contratacaoA.setQuantidade(2);
		contratacaoA.setDesconto(10.0);
		contratacaoA.setPrazo(1);

		this.mvc.perform(post("/api/contratacao/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(contratacaoA))).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void salvarContratacaoBSemDesconto() throws Exception {
		this.mvc.perform(post("/api/espaco/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(espaco2))).andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/cliente/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(cliente2)))
				.andExpect(MockMvcResultMatchers.status().isOk());

		Espaco espacoNoBanco =  new Espaco();
		espacoNoBanco.setId(1);
		Cliente clienteNoBanco = new Cliente();
		clienteNoBanco.setId(1);

		contratacaoB.setEspaco(espacoNoBanco);
		contratacaoB.setCliente(clienteNoBanco);
		contratacaoB.setTipoContratacao(TipoContratacao.DIARIA);
		contratacaoB.setQuantidade(1);
		contratacaoB.setPrazo(3);

		this.mvc.perform(post("/api/contratacao/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(contratacaoB))).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void calcularCustoContratacao() throws Exception {
		this.mvc.perform(post("/api/espaco/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(espaco3))).andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/cliente/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(cliente3)))
				.andExpect(MockMvcResultMatchers.status().isOk());

		Espaco espacoNoBanco =  new Espaco();
		espacoNoBanco.setId(1);
		Cliente clienteNoBanco = new Cliente();
		clienteNoBanco.setId(1);

		contratacaoC.setEspaco(espacoNoBanco);
		contratacaoC.setCliente(clienteNoBanco);
		contratacaoC.setTipoContratacao(TipoContratacao.DIARIA);
		contratacaoC.setQuantidade(3);
		contratacaoC.setDesconto(50.0);
		contratacaoC.setPrazo(1);

		MvcResult result = this.mvc.perform(post("/api/contratacao/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(contratacaoC))).andReturn();

		assertEquals("4750.0", result.getResponse().getContentAsString());
		//Custo hora = 200, Qtd contratada = 3 diaria, desconto=50 Total = 4750
	}

}
