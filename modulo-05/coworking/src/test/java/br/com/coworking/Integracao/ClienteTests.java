package br.com.coworking.Integracao;

import br.com.coworking.Controllers.ClienteController;
import br.com.coworking.Controllers.ContatoController;
import br.com.coworking.Controllers.TipoContatoController;
import br.com.coworking.CoworkingApplicationTests;
import br.com.coworking.Entity.Cliente;
import br.com.coworking.Entity.Contato;
import br.com.coworking.Entity.TipoContato;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
public class ClienteTests extends CoworkingApplicationTests {

	private MockMvc mvc;

	TipoContato email = new TipoContato();
	TipoContato facebook = new TipoContato();
	TipoContato telefone = new TipoContato();

	Contato contato1 = new Contato();
	Contato contato2 = new Contato();
	Contato contato3 = new Contato();

	Cliente cliente = new Cliente();
	Cliente cliente2 = new Cliente();

	@Autowired
	TipoContatoController tipoContatoController;

	@Autowired
	ContatoController contatoController;

	@Autowired
	ClienteController clienteController;

	@Autowired
	ApplicationContext context;

	@Autowired
	ObjectMapper mapper;

	@Before
	public void setUp() throws Exception{
		email.setNome("email");
		facebook.setNome("facebook");
		telefone.setNome("telefone");

		contato1.setValor("dbc@gmail.com");
		contato1.setTipoContato(new TipoContato());
		contato1.getTipoContato().setId(1);

		contato2.setValor("facebook/dbccompany");
		contato2.setTipoContato(new TipoContato());
		contato2.getTipoContato().setId(2);

		contato3.setValor("996959599");
		contato3.setTipoContato(new TipoContato());
		contato3.getTipoContato().setId(3);

		cliente.setNome("DBC");
		cliente.setCpf("0001001001");
		cliente.setDataNascimento(new Date(System.currentTimeMillis()));

		this.mvc = MockMvcBuilders.standaloneSetup(tipoContatoController, contatoController, clienteController).build();
		this.mvc.perform(post("/api/tipocontato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(email)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/tipocontato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(facebook)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/tipocontato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(telefone)))
				.andExpect(MockMvcResultMatchers.status().isOk());

		this.mvc.perform(post("/api/contato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(contato1)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/contato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(contato2)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		this.mvc.perform(post("/api/contato/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(contato3)))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void eBuscarTodos() throws Exception{
		this.mvc.perform(get("/api/cliente/"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void salvarClienteValido() throws Exception {
		for(int i = 1; i < 4; i++) {
			Contato contato = new Contato();
			contato.setId(i);
			cliente.pushContatos(contato);
		}

		MvcResult result = this.mvc.perform(post("/api/cliente/salvar").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(cliente)))
				.andReturn();

		assertEquals("Cliente salvo com sucesso", result.getResponse().getContentAsString());
	}

	@Test
	public void salvarClienteInvalido() throws Exception {
			Contato contato = new Contato();
			contato.setId(1);
			cliente2.pushContatos(contato);

		MvcResult result = this.mvc.perform(post("/api/cliente/salvar").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(cliente2))).andReturn();

		assertEquals("Cliente deve informar um contato do tipo email e um do tipo telefone", result.getResponse().getContentAsString());
	}


}
