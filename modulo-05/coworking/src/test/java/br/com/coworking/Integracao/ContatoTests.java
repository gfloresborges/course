package br.com.coworking.Integracao;

import br.com.coworking.Controllers.AcessoController;
import br.com.coworking.Controllers.ContatoController;
import br.com.coworking.CoworkingApplicationTests;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
public class ContatoTests extends CoworkingApplicationTests {

    private MockMvc mvc;

    @Autowired
    ContatoController contatoController;

    @Autowired
    ApplicationContext context;

    @Before
    public void setUp() {

        this.mvc = MockMvcBuilders.standaloneSetup(contatoController).build();
    }

    @Test
    public void eBuscarTodos() throws Exception{
        this.mvc.perform(get("/api/contato/"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
