package br.com.topsafe.Integracao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.topsafe.Entity.Estado;
import br.com.topsafe.Repository.EstadoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EstadoIntegration1Tests {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Test
	public void achaEstadoPorNome() {
		Estado estado = new Estado();
		estado.setNome("RS");
		entityManager.persist(estado);
		entityManager.flush();
		
		Estado found = estadoRepository.findByNome("RS");
		
		assertEquals("RS", found.getNome());
	}
}
