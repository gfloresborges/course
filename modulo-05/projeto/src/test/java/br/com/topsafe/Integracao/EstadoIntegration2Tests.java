package br.com.topsafe.Integracao;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.topsafe.Entity.Estado;
import br.com.topsafe.Repository.EstadoRepository;

@RunWith(SpringRunner.class)
public class EstadoIntegration2Tests {

	@MockBean
	private EstadoRepository estadoRepository;


	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		Estado estado = new Estado();
		estado.setNome("Rio Grande do Sul");
		
		Mockito.when(estadoRepository.findByNome(estado.getNome())).thenReturn(estado);
	}
	
	@Test
	public void achaEstadoPorNome2() {
		String nome = "Rio Grande do Sul";
		Estado found = estadoRepository.findByNome(nome);
		
		assertEquals("Rio Grande do Sul", found.getNome());
	}
}
