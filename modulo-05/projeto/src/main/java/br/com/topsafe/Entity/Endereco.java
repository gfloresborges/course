package br.com.topsafe.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "ENDERECO")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Endereco {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "ENDERECO_SEQ", sequenceName = "ENDERECO_SEQ")
	@GeneratedValue(generator = "ENDERECO_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(nullable = false)
	private String logradouro;
	
	@Column(nullable = false)
	private Integer numero;
	
	private String complemento;
	
	@ManyToOne
	@JoinColumn(name = "ID_BAIRRO")
	private Bairro bairro;
	
	@ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "ENDERECO_PESSOA",
        joinColumns = { @JoinColumn(name = "ID_ENDERECO")},
        inverseJoinColumns = { @JoinColumn(name = "ID_PESSOA")})
    private List<Pessoa> pessoas = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public List<Pessoa> getPessoas() {
		return pessoas;
	}

	public void pushPessoas(Pessoa... pessoas) {
		this.pessoas.addAll(Arrays.asList(pessoas));
	}
				
}
