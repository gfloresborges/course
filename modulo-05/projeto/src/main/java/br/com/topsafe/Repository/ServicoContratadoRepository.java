package br.com.topsafe.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.topsafe.Entity.Corretor;
import br.com.topsafe.Entity.Segurado;
import br.com.topsafe.Entity.Seguradora;
import br.com.topsafe.Entity.Servico;
import br.com.topsafe.Entity.ServicoContratado;

public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Integer>{
	List<ServicoContratado> findAllByDescricao(String descricao);
	List<ServicoContratado> findAllByValor(Double valor);
	List<ServicoContratado> findAllByServico(Servico servico);
	List<ServicoContratado> findAllBySeguradora(Seguradora seguradora);
	List<ServicoContratado> findAllBySegurado(Segurado segurado);
	List<ServicoContratado> findAllByCorretor(Corretor corretor);
}
