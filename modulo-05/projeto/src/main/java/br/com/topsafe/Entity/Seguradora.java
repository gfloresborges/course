package br.com.topsafe.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "SEGURADORA")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Seguradora {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "SEGURADORA_SEQ", sequenceName = "SEGURADORA_SEQ")
	@GeneratedValue(generator = "SEGURADORA_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(nullable = false)
	private String nome;
	
	@Column(nullable = false, unique = true)
	private String cnpj;
	
	@OneToOne
	@JoinColumn(name = "ID_ENDERECO_SEGURADORA")
	private EnderecoSeguradora enderecoSeguradora;
	
	@ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "SEGURADORA_SERVICO",
        joinColumns = { @JoinColumn(name = "ID_SEGURADORA")},
        inverseJoinColumns = { @JoinColumn(name = "ID_SERVICO")})
    private List<Servico> servicos = new ArrayList<>();
	
	@OneToOne(mappedBy = "seguradora")
	private ServicoContratado servicoContratado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public EnderecoSeguradora getEnderecoSeguradora() {
		return enderecoSeguradora;
	}

	public void setEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora) {
		this.enderecoSeguradora = enderecoSeguradora;
	}

	public List<Servico> getServicos() {
		return servicos;
	}

	public void pushServicos(Servico... servicos) {
		this.servicos.addAll(Arrays.asList(servicos));
	}

	public ServicoContratado getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(ServicoContratado servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
	
				
}
